from __future__ import print_function
from __future__ import division

import cantera as ct
import numpy as np
import matplotlib.pyplot as plt

# Plot results
plt.rcParams["figure.figsize"] = (4,4)
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_yscale('log')

aft71 = [2095.41488153, 2079.84951701, 2032.16704787, 2027.56736568]
d171 = [13.96620904, 13.72583086, 10.39923267,  9.07995677]

aft72 = [2129.27251699, 2118.87791614, 2069.01419368, 2065.81908804]
d172 = [ 8.23653658, 16.77324129, 10.55623861, 10.42963772]

aft73 = [2165.96379696, 2157.64790627, 2107.07346668, 2102.33374486]
d173 = [10.90792514, 19.88833743, 12.13758922, 12.43300787]

aft74 = [2204.91232097, 2191.53297199, 2145.58913827, 2140.82903366]
d174 = [15.70716872, 13.28828529, 15.11744596, 13.54580761]

aft75 = [2246.03361003, 2228.74866959, 2184.75511206, 2180.41061861]
d175 = [22.66822102, 18.65554101, 18.8971506 , 17.16975385]

#-------------

aft11 = [2493.69375651, 2472.25352379, 2411.16686118, 2404.5231874 ]
d111 = [ 692.14864604, 1056.33436847, 2429.22586822, 2200.69403716]

aft12 = [2525.89742643, 2504.33732512, 2443.02959865, 2436.44200789]
d112 = [ 659.19016015, 1068.89575247, 2244.53144327, 2331.48844976]

aft13 = [2557.50666466, 2536.18470773, 2469.53946515, 2468.34027676]
d113 = [ 614.02243539, 1056.86468075,  828.96333422, 2246.48234918]

aft14 = [2554.03468407, 2567.5016997,  2502.62267361, 2496.09318226]
d114 = [ 160.73540168,  996.68821811, 1057.48416357, 1024.23743754]

aft15 = [2588.29797795, 2598.65602932, 2535.3112747,  2529.17198254]
d115 = [ 211.36887125,  920.36214314, 1479.78565657, 1234.47710725]

#-------------

aft71_ = [2095.69184732, 2079.99406044, 2031.91508118, 2027.55782256]
d171_ = [13.81472782, 13.77646682, 10.17554585, 10.96847355]

aft72_ = [2129.92464192, 2119.55299709, 2069.05994792, 2065.6536303 ]
d172_ = [ 8.34481165, 16.97021453, 10.40502427, 10.90434188]

aft73_ = [2166.31084213, 2156.87098308, 2106.9648124 , 2102.33872926]
d173_ = [11.04678707, 19.7721564 , 11.95356387, 12.26363411]

aft74_ = [2204.43498669, 2191.38028284, 2145.30981698, 2140.57462447]
d174_ = [15.69607237, 13.33611436, 14.90682763, 13.33085729]

aft75_ = [2245.85156139, 2228.65446685, 2184.70240897, 2180.48771303]
d175_ = [22.69049709, 17.40614013, 18.72616852, 17.09249013]

#-------------

aft11_ = [2493.38159798, 2471.70186592, 2411.14074632, 2404.34052829]
d111_ = [ 692.14864604, 1056.33436847, 2429.22586822, 2200.69403716]


aft12_ = [2525.86270009, 2504.27697461, 2443.03867632, 2436.60682321]
d112_ = [ 662.71785853, 1073.29123286, 2241.08757673, 2500.9065203 ]


aft13_ = [2557.57176179, 2536.00653895, 2469.50424445, 2468.36234747]
d113_ = [ 617.34403905, 1058.01982446,  820.31783236, 2250.59961199]


aft14_ = [2553.86208888, 2567.39592955, 2502.60317238, 2496.10186451]
d114_ = [ 161.77042495,  998.88430888, 1051.43649491, 1015.86349139]


aft15_ = [2588.99246957, 2598.62683028, 2535.26848904, 2529.19494056]
d115_ = [ 219.89576181,  922.65335154, 1476.18538889, 1230.43881572]


#-------------

Temperatures = [580, 630, 680, 730, 780]

#-------------

phi07data = np.zeros(5,'d')
phi1data = np.zeros(5,'d')


phi07aft = np.zeros(5,'d')
phi1aft = np.zeros(5,'d')


#-------------

phi07data_ = np.zeros(5,'d')
phi1data_ = np.zeros(5,'d')


phi07aft_ = np.zeros(5,'d')
phi1aft_ = np.zeros(5,'d')

#-------------

n = 2

#-------------

phi07data[0] = d171[n]
phi07data[1] = d172[n]
phi07data[2] = d173[n]
phi07data[3] = d174[n]
phi07data[4] = d175[n]

phi1data[0] = d111[n]
phi1data[1] = d112[n]
phi1data[2] = d113[n]
phi1data[3] = d114[n]
phi1data[4] = d115[n]


#-------------

phi07aft[0] = aft71[n]
phi07aft[1] = aft72[n]
phi07aft[2] = aft73[n]
phi07aft[3] = aft74[n]
phi07aft[4] = aft75[n]

phi1aft[0] = aft11[n]
phi1aft[1] = aft12[n]
phi1aft[2] = aft13[n]
phi1aft[3] = aft14[n]
phi1aft[4] = aft15[n]


#-------------


phi07data_[0] = d171_[n]
phi07data_[1] = d172_[n]
phi07data_[2] = d173_[n]
phi07data_[3] = d174_[n]
phi07data_[4] = d175_[n]

phi1data_[0] = d111_[n]
phi1data_[1] = d112_[n]
phi1data_[2] = d113_[n]
phi1data_[3] = d114_[n]
phi1data_[4] = d115_[n]


#-------------

phi07aft_[0] = aft71_[n]
phi07aft_[1] = aft72_[n]
phi07aft_[2] = aft73_[n]
phi07aft_[3] = aft74_[n]
phi07aft_[4] = aft75_[n]

phi1aft_[0] = aft11_[n]
phi1aft_[1] = aft12_[n]
phi1aft_[2] = aft13_[n]
phi1aft_[3] = aft14_[n]
phi1aft_[4] = aft15_[n]


#-------------

lns1 = ax.plot(Temperatures, phi07data, '-s', label="$\phi$=0.7(CS)", color='black', linewidth=1)
lns2 = ax.plot(Temperatures, phi1data, '-v', label="$\phi$=1.0(CS)", color='black', linewidth=1)
lns6 = ax.plot(Temperatures, phi07data_, '-s', label="$\phi$=0.7(CS-Conv.)", color='blue', linewidth=1)
lns7 = ax.plot(Temperatures, phi1data_, '-v', label="$\phi$=1.0(CS-Conv.)", color='blue', linewidth=1)
ax2 = ax.twinx()
lns4 = ax2.plot(Temperatures, phi07aft, '-s', color='red', linewidth=1) #Flame Temp.($\phi$=0.7)
lns5 = ax2.plot(Temperatures, phi1aft, '-v', color='red', linewidth=1)
lns8 = ax2.plot(Temperatures, phi07aft_, '-s', color='blue', linewidth=1) #Flame Temp.($\phi$=0.7)
lns3 = ax2.plot(Temperatures, phi1aft_, '-v', color='blue', linewidth=1)

# added these three lines
lns = lns1+lns2+lns3+lns4+lns5+lns6+lns7+lns8
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc="upper left", prop={'size': 8})

ax2.spines['bottom'].set_color('red')
ax2.spines['top'].set_color('red')
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')

ax.grid()
ax.set_xlabel("Mixture temperature [K]")
ax.set_ylabel("$\mathregular{X_{N_{2}O}}$(max.)/$\mathregular{X_{N_{2}O}}$(99.9% AFT)")
ax2.set_ylabel("Flame Temperature [K]")
ax2.set_ylim(2000, 2800)
ax.set_ylim(1,10000)
ax.set_xlim(550,800)

plt.title(r'Emissions data ($\mathregular{E_{NH_{3}}}$=80%)',fontsize=10, horizontalalignment='center')
	      
plt.savefig('E80_1', bbox_inches='tight')



