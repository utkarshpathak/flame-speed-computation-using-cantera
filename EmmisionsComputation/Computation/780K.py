from __future__ import print_function
from __future__ import division

import cantera as ct
import numpy as np
import matplotlib.pyplot as plt
import pprint

print("Running Cantera Version: " + str(ct.__version__))

gastemp = 780.0
gaspres = 75.0
phi = 1.0

AmmoniaConcentrationByEnergyContent = [10, 20, 80, 90]

fractionOfFlameTemperature = 0.999 # NOx calcualtion at this fraction of adiabatic flame temperature

# Compute fuel mole fractions for a given set of energy fractions

NumberOfMixtures = len(AmmoniaConcentrationByEnergyContent); 
fuels = ['NULL'] * NumberOfMixtures
E = ['NULL'] * NumberOfMixtures

fillcount = 0
for ammonia in AmmoniaConcentrationByEnergyContent:
    if ammonia == 0 :
        fuels[fillcount] = 'NH3:0.0, NC7H16:1.0'
        E[fillcount] = '0%'
    elif ammonia == 100 :
        fuels[fillcount] = 'NH3:1.0, NC7H16:0.0'
        E[fillcount] = '100%'
    else :
        EnergyFractionAmmonia = ammonia/100.0
        fuels[fillcount] = 'NH3:1.0, NC7H16:'+str(0.0704089/EnergyFractionAmmonia) # NH3:1.0, NC7H16: (0.0704089 / E_NH3)
        E[fillcount] = str(AmmoniaConcentrationByEnergyContent[fillcount])+'%'
    fillcount = fillcount + 1
        
        
print(fuels)
print('\n\n')
print(E)	

# Initialize flame speed, flame thickness and flame temperature arrays
flamespeeds = np.zeros(NumberOfMixtures,'d')
flamethicknesses = np.zeros(NumberOfMixtures,'d')
flametemperatures = np.zeros(NumberOfMixtures,'d')

# Initialize N2O ratios
ratioN2OMaxtoN2OAdiabatic = np.zeros(NumberOfMixtures,'d')
ratioN2OMaxtoN2Oeq = np.zeros(NumberOfMixtures,'d')

oxidizer = 'O2:1.0, N2:3.76'
fuelcount = 0
for fuel in fuels:
    
	# Gas conditions
	gas = ct.Solution('cs1.cti')
	gas.TP = gastemp, gaspres*100000
	gas.set_equivalence_ratio(phi, fuel, oxidizer)

	print('\n\n')
	equivalenceratio = gas.get_equivalence_ratio()
	print('The equivalence ratio is: ' + str(equivalenceratio))
	print('\n\n')

	# Domain width in metres
	width = 0.000002

	# Create the flame object
	flame = ct.FreeFlame(gas, width=width)

	# Define tolerances for the solver
	flame.set_refine_criteria(ratio=3, slope=0.1, curve=0.1)

	# Define logging level
	loglevel = 1

	flame.solve(loglevel=loglevel, auto=True)
	Su0 = flame.u[0]
	
	print('\n\n')
	print(str(gaspres) + ' bar -> ')
	print("flame speed is: {:.2f} cm/s".format(Su0*100))
	print('\n\n')
	
	# Fill flame speed array
	flamespeeds[fuelcount] = Su0*100
	
	# Compute flame thickness and save in flame thickness array
	dtdxmax = [0, 0.0]
	totalpoints = len(flame.grid)
	dtdx = np.zeros(totalpoints-1,'d')
	for point in range(totalpoints-1):
		dtdx[point] = (flame.T[point+1]-flame.T[point])/(flame.grid[point+1]-flame.grid[point])
		if (dtdx[point] > dtdxmax[1]):
			dtdxmax[0] = point
			dtdxmax[1] = dtdx[point]
	flamethicknesses[fuelcount] = (flame.T[totalpoints-1]-flame.T[0])/dtdxmax[1]
	
	# Save adiabatic flame temperatures
	flametemperatures[fuelcount] = flame.T[totalpoints-1]
    

	# Plot results
	plt.rcParams["figure.figsize"] = (10,6)
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.set_yscale('log')

	# Extract concentration data
	X_N2O = flame.X[gas.species_index('N2O')]
	X_C7H16 = flame.X[gas.species_index('NC7H16')]
	X_NO = flame.X[gas.species_index('NO')]
	X_NH3 = flame.X[gas.species_index('NH3')]

	lns1 = ax.plot(flame.grid*1000, X_N2O*1000000, '-', label="$\mathregular{N_{2}O}$", color='black', linewidth=4)
	lns2 = ax.plot(flame.grid*1000, X_C7H16*1000000, '-', label="$\mathregular{C_{7}H_{16}}$", color='orange', linewidth=4)
	lns3 = ax.plot(flame.grid*1000, X_NO*1000000, '-', label="NO", color='blue', linewidth=4)
	lns4 = ax.plot(flame.grid*1000, X_NH3*1000000, '-', label="$\mathregular{NH_{3}}$", color='green', linewidth=4)
	ax2 = ax.twinx()
	lns5 = ax2.plot(flame.grid*1000, flame.T, '-', label=r'Temperature', color='red', linewidth=4)

	# added these three lines
	lns = lns1+lns2+lns3+lns4+lns5
	labs = [l.get_label() for l in lns]
	ax.legend(lns, labs, loc="lower right", prop={'size': 7})

	ax.grid()
	ax.set_xlabel("Distance [mm]")
	ax.set_ylabel("Mole fraction [ppm]")
	ax2.set_ylabel("Temperature [K]")
	ax2.set_ylim(500, 3000)
	ax.set_ylim(0.01,1000000)
	ax.set_xlim(0.0,0.2)

	plt.title(r'Emissions, Temp. vs. Distance ('+str(gaspres)+' bar, '+str(gastemp)+' K, $\phi$='+str(phi)+', $\mathregular{E_{NH_{3}}}$='+str(E[fuelcount])+', $\mathregular{S_{L}}$='+str('{0:.3g}'.format(Su0*100))+' cm/s)',fontsize=10, horizontalalignment='center')
		      
	#plt.savefig('Phi'+str(phi)+'_T'+str(gastemp)+'_ENH3_'+str(E[fuelcount])+'.png', bbox_inches='tight')
	
	# Compute N2O ratios
	
	# Compute N2O max
	N2Omax = 0.0
	for point in range(totalpoints):
		if (X_N2O[point] > N2Omax):
			N2Omax = X_N2O[point]
            
	# Compute N2O at T = given fraction of adiabatic flame temperature * Adiabatic Flame Temperature
	FindLocation = 0
	AdiabaticFlameTemperatureInto99Percent = flame.T[totalpoints-1] * fractionOfFlameTemperature
	for point in range(totalpoints):
		if (flame.T[point] > AdiabaticFlameTemperatureInto99Percent):
			FindLocation = point
			break
	N2OAdiabatic = X_N2O[FindLocation]
	N2Oeq = X_N2O[totalpoints-1] # at equilibrium
    
	ratioN2OMaxtoN2OAdiabatic[fuelcount] = N2Omax / N2OAdiabatic
	ratioN2OMaxtoN2Oeq[fuelcount] = N2Omax / N2Oeq
        	
	fuelcount = fuelcount + 1
	
print("The flame speeds are: ")
pprint.pprint(flamespeeds)
print('\n\n')
print("The flame thickness are: ")
pprint.pprint(flamethicknesses)
print('\n\n')
print("The flame temperatures are: ")
pprint.pprint(flametemperatures)
print('\n\n')
print("The ratioN2OMaxtoN2OAdiabatic are: ")
pprint.pprint(ratioN2OMaxtoN2OAdiabatic)
print('\n\n')
print("The ratioN2OMaxtoN2Oeq are: ")
pprint.pprint(ratioN2OMaxtoN2Oeq)

