from __future__ import print_function
from __future__ import division

import cantera as ct
import numpy as np
import matplotlib.pyplot as plt

# Plot results
plt.rcParams["figure.figsize"] = (6,6)
fig = plt.figure()
ax = fig.add_subplot(111)


#-------------

conc = [0,5,10,15,20,40,60,80,90,98,99.5,99.99,100]

#-------------

csdata = np.zeros(13,'d')
cndata = np.zeros(13,'d')
psdata = np.zeros(13,'d')
pndata = np.zeros(13,'d')

csaft = np.zeros(13,'d')
cnaft = np.zeros(13,'d')
psaft = np.zeros(13,'d')
pnaft = np.zeros(13,'d')

#-------------

csdata = [103.04498244,  90.86203853,  83.67287728,  78.11179969,  73.74858872,
  61.45984321,  53.80316352,  48.56395767,  46.54646572,  45.1254253,
  44.8772881,   44.80942303,  22.02968959]


csaft = [5.90060923e-06, 6.68777814e-06, 7.28062019e-06, 7.81292714e-06,
 8.44119607e-06, 1.02393932e-05, 1.17612129e-05, 1.30624932e-05,
 1.36371606e-05, 1.40665938e-05, 1.41441137e-05, 1.41665146e-05,
 2.67859270e-05]


cndata = [101.94294093,  89.11887718,  82.90194976,  78.7669082,   75.87701717,
  68.46723797,  64.33441077,  61.67711282,  60.65584382,  59.92057569,
  59.77377635,  59.73236743,  47.75604712]


cnaft = [5.97188096e-06, 6.86357054e-06, 7.41659358e-06, 7.83449581e-06,
 8.31567499e-06, 9.28361090e-06, 9.91479699e-06, 1.03797624e-05,
 1.05656431e-05, 1.06976235e-05, 1.07208462e-05, 1.07284944e-05,
 1.39225464e-05]


psdata = [83.18927187, 74.19046405, 68.58480335, 64.24099546, 60.5375056,  50.9996088,
 44.8840287,  40.74890469, 39.39293215, 38.28440509, 38.10424019, 38.0446305,
 20.07967548]


psaft = [6.94770693e-06, 7.83643413e-06, 8.42885353e-06, 8.96603256e-06,
 9.49010129e-06, 1.12887831e-05, 1.28353491e-05, 1.41821350e-05,
 1.47274638e-05, 1.51680079e-05, 1.52456770e-05, 1.52705857e-05,
 2.88110289e-05]

pndata = [84.12759657, 75.69752321, 71.20070934, 68.122636,   65.81013326, 60.23333018,
 57.28648671, 55.33860105, 54.62996691, 54.05362057, 53.96241961, 53.93305283,
 46.11593715]


pnaft = [6.88749700e-06, 7.79959010e-06, 8.29794524e-06, 8.67253840e-06,
 8.98259300e-06, 9.88667319e-06, 1.04782853e-05, 1.09084107e-05,
 1.10791835e-05, 1.12116617e-05, 1.12336220e-05, 1.12407322e-05,
 1.40769136e-05]


mplsdata = [47.11466042, 47.13397471, 46.64060544, 45.98287135, 45.23678576, 41.97201699,
 39.29422043, 36.91790094, 35.86250396, 35.11721137, 34.9834859,  34.94022295,
 18.99650475]


mplsaft = [1.30386539e-05, 1.30726680e-05, 1.31941935e-05, 1.33588917e-05,
 1.35465256e-05, 1.45662958e-05, 1.57411710e-05, 1.67203100e-05,
 1.71994446e-05, 1.75541143e-05, 1.76207417e-05, 1.76424972e-05,
 3.08825267e-05]

#-------------

n = 13

for i in range(n):
	csaft[i] = csaft[i] * 1000.0
	cnaft[i] = cnaft[i] * 1000.0
	psaft[i] = psaft[i] * 1000.0
	pnaft[i] = pnaft[i] * 1000.0
	mplsaft[i] = mplsaft[i] * 1000.0
		

lns1 = ax.plot(conc, csdata, '-s', label="Chalmers-Stagni", color='black', linewidth=1)
lns2 = ax.plot(conc, cndata, '-v', label="Chalmers-Nozari", color='black', linewidth=1)
lns3 = ax.plot(conc, psdata, '-^', label="Pichler-Stagni", color='black', linewidth=1)
lns4 = ax.plot(conc, pndata, '-o', label="Pichler-Nozari", color='black', linewidth=1)
lns9 = ax.plot(conc, mplsdata, '-x', label="MPLS", color='black', linewidth=1)
ax2 = ax.twinx()
lns5 = ax2.plot(conc, csaft, '-s', color='red', linewidth=1) 
lns6 = ax2.plot(conc, cnaft, '-v', color='red', linewidth=1)
lns7 = ax2.plot(conc, psaft, '-^', color='red', linewidth=1)
lns8 = ax2.plot(conc, pnaft, '-o', color='red', linewidth=1)
lns10 = ax2.plot(conc, mplsaft, '-x', color='red', linewidth=1)

# added these three lines
lns = lns1+lns2+lns3+lns4+lns5+lns6+lns7+lns8+lns9+lns10
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc="upper center", prop={'size': 10})

ax2.spines['bottom'].set_color('red')
ax2.spines['top'].set_color('red')
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')

ax.grid()
ax.set_xlabel("Ammonia (energy contribution) [%]")
ax.set_ylabel("Flame Speed [cm/s]")
ax2.set_ylabel("Flame Thickness [mm]")
#ax2.set_ylim(2000, 2800)
#ax.set_ylim(1,10000)
#ax.set_xlim(550,800)

plt.title(r'Flame speed vs. Ammonia concentration',fontsize=10, horizontalalignment='center')
	      
plt.savefig('Flamedata', bbox_inches='tight')



